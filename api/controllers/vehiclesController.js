// Created By Bipul Wagle;
// House Vehicle Model's controller used in the router.

'use strict'

var responseUtil = require('../lib/responseUtil');

var exampleResponse = {
    title : "responseWorking",
    message : "response message working"
};

//fetches logged in house's vehicles.
exports.list_house_vehicles = function(req, res, app){
    app.get("models").HouseVehicle.findAll({
        where:{
            houseId : req.house.id
        }
    }).then(vehicles=>{
        var resultVehicles = [];
        vehicles.forEach(function(element){
            resultVehicles.push(element.dataValues);
        });
        responseUtil.success(res, resultVehicles);
    }, onRejected=>{
        responseUtil.dbRejectedResponse(res, onRejected);
    });
};

//create a house vehicle for the logged in house.
exports.create_house_vehicle = function(req, res, app){
    if(!req.body.data.memberId){
        responseUtil.error(res, "Undefined Member ID");
    }
    app.get("models").HouseMember.findById(req.body.data.memberId).then(tempHouseModel=>{
        app.get("models").HouseVehicle.create({
            make: req.body.data.make,
            model: req.body.data.model,
            year: req.body.data.year,
            license_plate: req.body.data.license_plate,
            houseId: req.house.id,
            memberId: req.body.data.memberId
        }).then(onResolved=>{
            responseUtil.success(res, onResolved.dataValues);
        }, onRejected=>{
            responseUtil.dbRejectedResponse(res, onRejected);
        });
    }, onRejected=>{
        responseUtil.dbRejectedResponse(res, onRejected);
    });
};


//returns vehicle info of the house with given params vehicle's "id".
exports.list_a_house_vehicle = function(req, res, app){
    console.log(req.params.id);
    app.get("models").HouseVehicle.findOne({
        where:{
            id : req.params.id
        }
    }).then(onResolve=>{
        // console.log(onResolve);
        if(onResolve){
            responseUtil.success(res, onResolve.dataValues);
        }else{
            responseUtil.success(res, {});
        }
    }, onRejected=>{
        responseUtil.dbRejectedResponse(res, onRejected);
    });
}


