// Created By Bipul Wagle;
// House Member Model's controller used in the router.

'use strict'


var responseUtil = require('../lib/responseUtil');

//fetches logged in house's members.
exports.list_house_members = function(req, res, app){
    
    app.get("models").HouseMember.findAll({where:{
        houseId : req.house.id 
    }}).then(members=>{
        var tempMembers = [];
        members.forEach(function(element){
            tempMembers.push(element.dataValues);
        });
        responseUtil.success(res, tempMembers);
    }, onRejected=>{
        responseUtil.dbRejectedResponse(res, onRejected);
    });
};


//create a house members for the logged in house.
exports.create_house_member = function(req, res, app){

    app.get("models").House.findById(req.house.id).then(tempHouseModel=>{
        app.get("models").HouseMember.create({
            first_name: req.body.data.first_name,
            last_name : req.body.data.last_name,
            email: req.body.data.email,
            age : req.body.data.age,
            gender: req.body.data.gender,
            houseId: tempHouseModel.dataValues.id
        }).then(onResolved=>{
            console.log(onResolved.dataValues);
            var result = onResolved.dataValues;
            responseUtil.success(res, result);
        }, onRejected=>{
            responseUtil.dbRejectedResponse(res, onRejected);
        });
    }, onRejected=>{
        responseUtil.error(res, "Invalid House for Member");
    });
};

//returns member info of the house with given params id.
exports.list_a_house_member = function(req, res, app){
    app.get("models").HouseMember.findById(req.params.id).then(onResolved=>{
        responseUtil.success(res, onResolved.dataValues);
    }, onRejected=>{
        responseUtil.dbRejectedResponse(res, onRejected);
    });
}
