// Created By Bipul Wagle;
// House Model's controller used in the router.

'use strict'


var jwt    = require('jsonwebtoken');
var responseUtil = require('../lib/responseUtil');
var bcrypt = require('bcrypt');


const saltRounds = 3;


//fetches all houses matching input house address.
//compares passwords and returns the house that matches the password along with the jwt token.

exports.sign_in = function(req, res, app){
    console.log(req.body);
    if(!req.body || !req.body.address){
        responseUtil.error(res, "Undefined Input Address");
        return;
    }
    if(!req.body.password){
        responseUtil.error(res, "Undefined Password");
        return;
    }

    app.get('models').House.findAll({
        where: {
            address: req.body.address.toLowerCase() 
        }
    }).then(houses =>{
        console.log(houses);
        if (houses.length > 0){
            var matchedHouse;
            
            houses.forEach(function(element){
                if(!matchedHouse && bcrypt.compareSync(req.body.password, element.dataValues.password)){
                    matchedHouse = element;
                    matchedHouse.password = "";
                    console.log(1);
                    var input_token = generateJWTToken(app, matchedHouse);
                    responseUtil.tokenResponse(res, input_token, matchedHouse);
                    return;
                } 
            });

            if(!matchedHouse){
                console.log(2);
                responseUtil.error(res, "Invalid Username or Password2");
                return;
            }
        }else{
            console.log(3);
            responseUtil.error(res, "Invalid Username or Password1");
            return;
        }
        
    }, onRejected =>{
        responseUtil.error(res, onRejected);
    });
}


// hashes password and stores in database. returns response with jwt token and created house.

exports.create_house = function(req, res, app){
    if(!req.body.address){
        responseUtil.error(res, "Please Enter a Address");
        return;
    }
    if(!req.body.password){
        responseUtil.error(res, "Please enter a Password");
        return;
    }

    bcrypt.hash(req.body.password, saltRounds).then(function(hash){
        var inputDataModel = {
            password: hash,
            address: req.body.address.toLowerCase(),
            zip: req.body.zip,
            city: req.body.city,
            state: req.body.state,
            number_of_bedrooms: req.body.number_of_bedrooms
        }
        app.get('models').House.create(inputDataModel).then(onResolve=>{
            onResolve.password = "";
            var token = generateJWTToken(app, onResolve);
            responseUtil.tokenResponse(res, token, onResolve);
            // responseUtil.success(res, onResolve);
        }, onRejected=>{
            if(onRejected.error){
                responseUtil.error(res, onRejected.error.message);
            }else if(onRejected.errors){
                responseUtil.error(res, onRejected.errors[0].message);
            }
        });
    });
    
};


//returns house info of the house with given params id.
exports.list_a_house = function(req, res, app){
    console.log(req.params , req.house);
    if (req.params.id == req.house.id){
        app.get('models').House.findById(req.params.id).then(onResolved=>{
            onResolved.password = "";
            responseUtil.success(res, onResolved);
        }, onRejected=>{
            responseUtil.error(res, onRejected);
        }); 
    }else{
        responseUtil.error(res, "Tampered Request Error");
    }
       
}



//use passed house object to generate jwt token.
//app is used to get the global JWT-SECRET string.
function generateJWTToken(app, house){
    var tempHouse = {
        address: house.address,
        zip: house.zip,
        city: house.city,
        state: house.state,
        number_of_bedrooms: house.number_of_bedrooms,
        id : house.id
    }

    return jwt.sign(tempHouse, app.get("JWT-SECRET"), {
        expiresIn: 60*60*24
    });

}