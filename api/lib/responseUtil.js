// Created By Bipul Wagle;
// General Response Util Library. Helps centralize json response format.

'use strict'


exports.error = function(res,error){
    if(typeof error == "string"){
        res.send({
            error: true,
            error_msg: error
        });
    }else{
        res.send({
            error: true,
            data: error
        });
    }
}

exports.success = function(res, input_data){
    if(typeof input_data == "string"){
        res.send({
            error: false,
            data:{
                message: input_data
            }
        })
    }else{
        res.send({
            error: false,
            data: input_data
        })
    }
}

exports.tokenResponse = function(res,input_token, input_data){
    var result = {
        error: false,
        token : input_token,
        data : input_data
    }

    res.send(result);
}

exports.dbRejectedResponse = function(res, onRejected){
    if(onRejected.error){
        this.error(res, onRejected.error.message);
    }else if(onRejected.errors){
        this.error(res, onRejected.errors[0].message);
    }
}