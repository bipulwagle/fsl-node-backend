// Created By Bipul Wagle;
// House Model.

'use strict'

module.exports = function(sequelize, DataTypes){
    var House = sequelize.define('House', {
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address:{
            type: DataTypes.STRING,
            allowNull: false
        },
        zip:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false
        },
        state:{
            type: DataTypes.STRING,
            allowNull: false
        },
        number_of_bedrooms: {
            type: DataTypes.INTEGER,
            allowNull: false
        }

    },{
        classMethods:{
        }
    });
    return House;
}