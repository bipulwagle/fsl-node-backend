// Created By Bipul Wagle;
// House Member Model.

'use strict'

module.exports = function(sequelize, DataTypes){
    var House = sequelize.import('house');
    var HouseMember = sequelize.define('HouseMember', {
        first_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        last_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        gender: {
            type: DataTypes.STRING,
            allowNull: false
        },
        houseId: {type: DataTypes.INTEGER,
            allowNull: false, 
            references: {
                model: House,
                key: 'id',
            }
        }
    },{
        classMethods:{
            
        }
    });
    return HouseMember;
}
