// Created By Bipul Wagle;
// House Vehicle Model.


'use strict'

module.exports = function(sequelize, DataTypes){
    var House = sequelize.import('house');
    var HouseMember = sequelize.import('houseMember');
    
    var HouseVehicle = sequelize.define('HouseVehicle', {
        make: {
            type: DataTypes.STRING,
            allowNull: false
        },
        model: {
            type: DataTypes.STRING,
            allowNull: false
        },
        year: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        license_plate: {
            type: DataTypes.STRING,
            allowNull: false
        },
        houseId: {type: DataTypes.INTEGER, 
            allowNull: false,
            references: {
                model: House,
                key: 'id',
            }
        },
        memberId: {type: DataTypes.INTEGER,
            allowNull: false, 
            references: {
                model: HouseMember,
                key: 'id',
            }
        }

    },{
        classMethods:{

        }
    });
    return HouseVehicle;
}
