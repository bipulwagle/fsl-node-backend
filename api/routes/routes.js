// Created By Bipul Wagle;
// Determines routes for the backend api. Uses JWT middleware to validate authentication status.

'use strict'

var responseUtil = require('../lib/responseUtil');
var cors = require('cors');
var jwt    = require('jsonwebtoken');

module.exports = function(app, express){
        var houseController = require('../controllers/housesController');
        var membersController = require('../controllers/membersController');
        var vehicleController = require('../controllers/vehiclesController');

        


        //base un-authenticated routes;
        var baseRoutes = express.Router();
        app.use(cors());
        baseRoutes.post('/signin', function(req,res){houseController.sign_in(req,res,app)});
        baseRoutes.post('/houses', function(req,res){houseController.create_house(req,res,app)});

        app.use('/', baseRoutes);

        


        //jwt validation
        app.use(function(req, res, next){
                var token = req.body.token || req.query.token;
                if(!token){
                        responseUtil.error(res, "Not Logged In");
                        return;
                }

                jwt.verify(token, app.get('JWT-SECRET'), function(err, house){
                        if(err){
                                responseUtil.error(res, "Not Logged In")
                        }else{
                                console.log(house);
                                req.house = house;
                                next();
                        }
                })
                
        });

        //authenticated routes;
        var apiRoutes = express.Router();
        apiRoutes.get('/houses/:id',function(req,res){houseController.list_a_house(req,res,app)});

        
        apiRoutes.get('/members', function(req,res){membersController.list_house_members(req,res,app)});
        apiRoutes.post('/members', function(req,res){membersController.create_house_member(req,res,app)});
        apiRoutes.get('/members/:id', function(req,res){membersController.list_a_house_member(req,res,app)});
       
        apiRoutes.get('/vehicles', function(req,res){vehicleController.list_house_vehicles(req,res,app)});
        apiRoutes.post('/vehicles', function(req,res){vehicleController.create_house_vehicle(req,res,app)});
        apiRoutes.get('/vehicles/:id', function(req,res){vehicleController.list_a_house_vehicle(req,res,app)});
       
        app.use('/api', apiRoutes);
};