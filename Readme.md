Installation Instructions!
===============

Database Setup
--------------

**Install postgresql for your system.**

Create 

* Database : 'fullstacklabsexample',
* User : 'fullstackProjectAdmin' 
* Password: 'fullstackprojectpassword1'

User 'fullstackProjectAdmin' should have all privileges on the database 'fullstacklabsexample'.


Server Setup
------------

    git clone https://bitbucket.org/bipulwagle/fsl-node-backend

    cd fsl-node-backend

Installing Dependencies

    npm install --save

In server.js set models sync force to true, to create tables in databasse. 
E.g. 

    models.sequelize.sync({force:true});

Then start the server using:

    npm server "run"

kill the server application using 

    ctr+c

In server.js set models sync force to false, to use tables created in the steps above. E.g. 

    models.sequelize.sync({force:false});

Start using the server using:

    npm server "run"

backend server should now be serving on **http://localhost:8080**


