
var express = require('express'),
	app = express(),
	port = process.env.PORT || 8080;

var bodyParser  = require('body-parser');

var routes = require('./api/routes/routes');
var models = require('./api/models/index');

app.set("JWT-SECRET", "FSL Example JWT Pass")
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
models.sequelize.sync({force:true});


app.set("models", models);
routes(app, express);
app.listen(port);



console.log('Full Stack Labs Example RESTful API server started on: ' + port);

